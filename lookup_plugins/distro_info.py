from ansible.errors import AnsibleError
from ansible.plugins.lookup import LookupBase

DOCUMENTATION = """
    lookup: distro_info
    author: Raphaël Hertzog
    short_description: returns info about debian-based distributions
    description:
      - Returns a list of dict; for each distribution codename you pass in, returns a dict with information about that distribution.
    options:
      _terms:
        description: list of distributions
"""

EXAMPLES = """
- name: show details of a distribution
  debug:
    msg: "{{ lookup('distro_info', 'ubuntu:focal') }}"

In a template:
{% for dist in ['debian:buster', 'buster-security'] %}
{{ lookup('distro_info', dist).sources_list_entry }}
{% endfor %}
"""

RETURN = """
_raw:
   description: file(s) content after templating
   type: list
   elements: dict
"""


class LookupModule(LookupBase):

    def _find_dist(self, dist, vendor=None):
        if vendor:
            iterator = [(vendor, self._data.get(vendor, {}))]
        else:
            iterator = self._data.items()

        for _vendor, _vendor_data in iterator:
            for _dist, _dist_data in _vendor_data['dists'].items():
                if _dist == dist or dist in _dist_data.get('aliases', []):
                    return (_vendor, _dist)

        if vendor:
            if vendor not in self._data:
                raise AnsibleError('Vendor %s is unknown' % vendor)
            else:
                raise AnsibleError(
                    'Unknown distribution %s for vendor %s' % (dist, vendor)
                )
        else:
            raise AnsibleError('Unknown distribution %s' % dist)

    def _identify_vendor_and_dist(self, term):
        (vendor, dist) = (None, term)
        if ":" in term:
            (vendor, dist) = term.split(":", 1)

        (vendor, dist) = self._find_dist(dist, vendor=vendor)

        return (vendor, dist)

    def run(self, terms, variables, **kwargs):

        try:
            self._data = variables['distro_info']
        except KeyError:
            raise AnsibleError("Could not find 'distro_info' variable")

        ret = []
        for term in terms:
            (vendor, dist) = self._identify_vendor_and_dist(term)

            vendor_data = self._data[vendor]
            dist_data = vendor_data['dists'][dist]
            repository = dist_data.get('repository', 'default')
            try:
                repo_data = vendor_data['repositories'][repository]
            except KeyError:
                raise AnsibleError('Repository %s is unknown for vendor %s' %
                                   (repository, vendor))

            def _lookup(field, default=None):
                return dist_data.get(
                    field,
                    repo_data.get(
                        field,
                        vendor_data.get(field, default)
                    )
                )

            repo_mirror = self._templar.template(
                _lookup("mirror"), fail_on_undefined=True)
            repo_components = _lookup("components")
            sources_list_entry = "%s %s %s" % (
                repo_mirror,
                dist_data.get("apt_codename", dist),
                " ".join(repo_components),
            )

            result = {
                "codename": dist,
                "vendor": vendor,
                "repository": {
                    "mirror": repo_mirror,
                    "components": repo_components,
                    "keyring_package": _lookup("keyring_package", ""),
                    "keyring_file": _lookup("keyring_file", ""),
                },
                "debootstrap_options": _lookup("debootstrap_options", ""),
                "debootstrap_script": _lookup("debootstrap_script", ""),
                "aliases": dist_data.get("aliases", []),
                "parent": dist_data.get("parent", None),
                "binary_apt_entry": "deb " + sources_list_entry,
                "source_apt_entry": "deb-src " + sources_list_entry,
            }

            ret.append(result)

        return ret
